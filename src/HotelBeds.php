<?php 

namespace HotelBeds;

use GuzzleHttp\Exception\BadResponseException;
use HotelBeds\Exceptions\HotelBedsException;
use HttpException;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

abstract class HotelBeds {

	protected $endpoint;
	protected $key;
	protected $secret;

    /**
     * @param $method
     * @param $url
     * @param array $request
     * @return mixed|ResponseInterface
     * @throws HotelBedsException
     * @throws HttpException
     */
    protected function send($method, $url, $request = [])
	{
		$client = $this->createClient();

		try{
		    $body = [];

		    if($method == 'POST' || $method == 'PUT'){
                $body = ['body' => json_encode($request)];
            }

            $response = $client->request($method, $this->endpoint . $url, $body);

        }catch (BadResponseException $e)
        {
		    $exception =json_decode($e->getResponse()->getBody()->getContents());

            throw new HotelBedsException($exception->error->message, $e->getCode());
        }

		return $this->parseResponse($response);

	}

    /**
     * @return Client
     */
    private function createClient()
	{
		return new Client([
			'headers' => [
				'Accept-Encoding'	=> 'gzip',
				'Content-Type'		=> 'application/json',
				'Accept'			=> 'application/json',
				'Api-Key'			=> $this->key,
				'X-Signature'		=> $this->buildSignature()
			]
		]);
	}

	private function buildSignature()
	{
		return hash('sha256', $this->key.$this->secret.time());
	}


    /**
     * @param ResponseInterface $response
     * @return mixed|ResponseInterface
     * @throws HotelBedsException
     * @throws HttpException
     */
    private function parseResponse(ResponseInterface $response)
	{
        if($response->getStatusCode() != 200){
            throw new HttpException($response->getBody());
        }

        $response = $this->toArray($response);

        if(isset($response->error)){
            throw new HotelBedsException($response->error->message, $response->error->code);
        }

        return $response;
	}

    /**
     * @param ResponseInterface $response
     * @return mixed
     */
    private function toArray(ResponseInterface $response)
    {
        return json_decode($response->getBody()->getContents());
    }

    protected function buildRequest($request)
    {
        return http_build_query($request);
    }

}
