<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 23/01/2019
 * Time: 12:25 PM
 */

namespace HotelBeds\Hotel;


use HotelBeds\Hotel\Contracts\HotelsContentContract;
use HotelBeds\HotelBeds;

class HotelsContent extends HotelBeds implements HotelsContentContract
{

    public function __construct($endpoint, $key, $secret)
    {
        $this->endpoint = $endpoint;
        $this->key      = $key;
        $this->secret   = $secret;
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getHotels($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/hotels?'.$request);
    }

    /**
     * @param $code
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getHotelDetails($code, $request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET',"/hotel-content-api/1.0/hotels/{$code}?".$request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getCountries($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/locations/countries?' . $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getDestinations($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/locations/destinations?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getAccommodations($request)
    {
        $request = $this->buildRequest($request);

       return $this->send('GET','/hotel-content-api/1.0/types/accommodations?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getBoards($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/boards?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getCategories($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/categories?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getChains($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/chains?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getCurrencies($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/currencies?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getFacilities($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/facilities?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getFacilityGroups($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/facilitygroups?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getFacilityTypologies($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/facilitytypologies?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getIssues($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/issues?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getLanguages($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/languages?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getPromotions($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/promotions?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getRooms($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/rooms?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getSegments($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/segments?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getTerminals($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/terminals?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getImageTypes($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/imagetypes?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getGroupCategories($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/groupcategories?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getRateComments($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/ratecomments?'. $request);
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function getRateCommentDetails($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-content-api/1.0/types/ratecommentdetails?'. $request);
    }
}