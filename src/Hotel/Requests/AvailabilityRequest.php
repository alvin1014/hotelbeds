<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 26/01/2019
 * Time: 1:32 PM
 */

namespace HotelBeds\Hotel\Requests;


use HotelBeds\Requests\HotelBedsRequest;

class AvailabilityRequest extends HotelBedsRequest
{
    protected $availability = [];

    public function __construct()
    {
        $this->setBoards();
        $this->setFilters();
        $this->availability['dailyRate'] = true;
    }

    public function setStay($checkIn, $checkOut, $shiftDays)
    {
        $stay = [
            'checkIn'           => date('Y-m-d', strtotime($checkIn)),
            'checkOut'          => date('Y-m-d', strtotime($checkOut)),
            'shiftDays'         => $shiftDays,
            'allowOnlyShift'    => 'false'
        ];

        $this->availability['stay'] = $stay;

        return $this;
    }

    public function setOccupancies($occupancies)
    {
        $occupancy = [];

        foreach($occupancies as $value)
        {
            $ages = isset($value['ages']) ? $value['ages'] : [];

            $occupancy[] = [
                'rooms'     => $value['rooms'],
                'adults'    => $value['adults'],
                'children'  => $value['children'],
                'paxes'     => $this->setPaxes(['adults' => $value['adults'], 'children' => $value['children']],$ages)
            ];
        }

        $this->availability['occupancies'] = $occupancy;

        return $this;
    }

    private function setPaxes($paxes,$ages)
    {
        $pax = [];

        foreach ($paxes as $key => $data){

            for($i=0;$i<$data;$i++){
                if($key == 'adults'){
                    $pax[] = ['type' => 'AD'];
                }

                if($key == 'children'){
                    $pax[] = ['type' => 'CH','age'  => $ages[$i]];
                }
            }
        }

        return $pax;
    }

    public function setHotels($hotels)
    {
        $this->availability['hotels'] = [
            'hotel' => $hotels
        ];

        return $this;
    }

    public function setBoards($boards = ['RO','BB','AI'])
    {
        $this->availability['boards'] = [
            'included'  => true,
            'board'     => $boards
        ];

        return $this;
    }

    public function setRooms($rooms = [])
    {
        $this->availability['rooms'] = [
            'included' => true,
            'room'  => $rooms
        ];

        return $this;
    }

    public function setAccommodation($accommodations = ['HOTEL','APARTMENT'])
    {
        $this->availability['accommodations'] = $accommodations;

        return $this;
    }

    public function setReviews($reviews = [])
    {
        $this->availability['reviews'] = $reviews;

        return $this;

    }

    public function setFilters($filter = [])
    {
        $this->availability['filter'] = $filter;

        $this->availability['filter']['packaging'] = true;
        $this->availability['filter']['paymentType'] = 'AT_WEB';

        return $this;
    }
}