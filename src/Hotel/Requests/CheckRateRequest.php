<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 30/01/2019
 * Time: 2:16 PM
 */

namespace HotelBeds\Hotel\Requests;


use HotelBeds\Requests\HotelBedsRequest;

class CheckRateRequest extends HotelBedsRequest
{
    protected $rooms = [];

    public function setRateKey($rates)
    {
        $this->rooms = $rates;

        return $this;
    }
}