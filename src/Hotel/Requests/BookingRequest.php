<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 30/01/2019
 * Time: 3:59 PM
 */

namespace HotelBeds\Hotel\Requests;


use HotelBeds\Requests\HotelBedsRequest;

class BookingRequest extends HotelBedsRequest
{
    protected $holder   = [];
    protected $rooms    = [];
    protected $clientReference;
    protected $tolerance;

    public function __construct()
    {
        $this->clientReference = "rainalbert";
        $this->tolerance = 2;
    }

    public function setHolder($holder)
    {
        $this->holder = $holder;

        return $this;
    }

    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }
}