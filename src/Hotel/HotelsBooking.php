<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 26/01/2019
 * Time: 12:46 PM
 */

namespace HotelBeds\Hotel;


use HotelBeds\Hotel\Contracts\HotelsBookingContract;
use HotelBeds\Hotel\Requests\AvailabilityRequest;
use HotelBeds\Hotel\Requests\BookingRequest;
use HotelBeds\Hotel\Requests\CheckRateRequest;
use HotelBeds\HotelBeds;

class HotelsBooking extends HotelBeds implements HotelsBookingContract
{
    public function __construct($endpoint, $key, $secret)
    {
        $this->endpoint = $endpoint;
        $this->key      = $key;
        $this->secret   = $secret;
    }

    /**
     * @param AvailabilityRequest $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function availability(AvailabilityRequest $request)
    {
        $availability = $request->get()['availability'];

        return $this->send('POST','/hotel-api/1.0/hotels',$availability);
    }

    /**
     * @param CheckRateRequest $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function checkRates(CheckRateRequest $request)
    {
        return $this->send('POST','/hotel-api/1.0/checkrates', $request->get());
    }

    /**
     * @param BookingRequest $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function booking(BookingRequest $request)
    {
        return $this->send('POST','/hotel-api/1.0/bookings', $request->get());
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function bookingList($request)
    {
        $request = $this->buildRequest($request);

        return $this->send('GET','/hotel-api/1.0/bookings?'.$request);
    }

    /**
     * @param $reference
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function bookingDetail($reference)
    {
        return $this->send('GET','/hotel-api/1.0/bookings/'.$reference);
    }

    /**
     * @param $reference
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \HotelBeds\Exceptions\HotelBedsException
     * @throws \HttpException
     */
    public function bookingCancellation($reference, $request)
    {
        $request = $this->buildRequest($request);

        return $this->send('DELETE','/hotel-api/1.0/bookings/'.$reference.'?'.$request);
    }

}