<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 23/01/2019
 * Time: 12:16 PM
 */

namespace HotelBeds\Hotel\Contracts;


interface HotelsContentContract
{
    public function getHotels($request);

    public function getHotelDetails($code, $request);

    public function getCountries($request);

    public function getDestinations($request);

    public function getAccommodations($request);

    public function getBoards($request);

    public function getCategories($request);

    public function getChains($request);

    public function getCurrencies($request);

    public function getFacilities($request);

    public function getFacilityGroups($request);

    public function getFacilityTypologies($request);

    public function getIssues($request);

    public function getLanguages($request);

    public function getPromotions($request);

    public function getRooms($request);

    public function getSegments($request);

    public function getTerminals($request);

    public function getImageTypes($request);

    public function getGroupCategories($request);

    public function getRateComments($request);

    public function getRateCommentDetails($request);
}