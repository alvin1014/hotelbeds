<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 26/01/2019
 * Time: 12:46 PM
 */

namespace HotelBeds\Hotel\Contracts;


use HotelBeds\Hotel\Requests\AvailabilityRequest;
use HotelBeds\Hotel\Requests\BookingRequest;
use HotelBeds\Hotel\Requests\CheckRateRequest;

interface HotelsBookingContract
{
    public function availability(AvailabilityRequest $request);

    public function checkRates(CheckRateRequest $request);

    public function booking(BookingRequest $request);

    public function bookingList($request);

    public function bookingDetail($reference);

    public function bookingCancellation($reference, $request);
}