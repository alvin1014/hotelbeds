<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 26/01/2019
 * Time: 1:34 PM
 */

namespace HotelBeds\Requests;


class HotelBedsRequest
{
    public function get()
    {
        return get_object_vars($this);
    }
}