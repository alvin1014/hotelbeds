<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 30/01/2019
 * Time: 2:31 PM
 */

namespace tests;


use HotelBeds\Hotel\HotelsBooking;
use HotelBeds\Hotel\Requests\AvailabilityRequest;
use PHPUnit\Framework\TestCase;

class HotelTestCase extends TestCase
{
    public $repository;

    public function setUp()
    {
        parent::setUp();
    }

    public function getAvailability()
    {
        $search = [
            'hotels' => [85248,85249,85250,85251,85252,85253,85254,85255,85256,85257,85258,85259,85260],
            'stay'  => [
                'checkIn'   => date('Y-m-d', strtotime("+7 day")),
                'checkOut'  => date('Y-m-d', strtotime("+9 day")),
                'shiftDays' => 2,
            ],
            'occupancies' => [
                0 => [
                    'rooms'     => 1,
                    'adults'    => 2,
                    'children'  => 1,
                    'ages'      => [
                        0 => 5
                    ]
                ]
            ]
        ];


        $request = new AvailabilityRequest();

        $request->setStay($search['stay']['checkIn'], $search['stay']['checkOut'], $search['stay']['shiftDays'])
            ->setOccupancies($search['occupancies'])
            ->setHotels($search['hotels'])
            ->setBoards(['RO']);

        $response = $this->repository->availability($request);

        return $response;
    }
}