<?php



use HotelBeds\Hotel\HotelsBooking;
use PHPUnit\Framework\TestCase;
use HotelBeds\Hotel\HotelsContent;
use tests\HotelTestCase;

class HotelsContentTest extends HotelTestCase {

    /**
     *
     */
    private $request;

    public function setUp()
    {
        parent::setUp();

        \Dotenv\Dotenv::create(__DIR__,'../../.env')->load();

        $this->repository = new HotelsContent(getenv('HOTELBEDS_URL'),getenv('HOTELBEDS_KEY'),getenv('HOTELBEDS_SECRET'));
        $this->request = [
            'fields'    => 'all',
            'from'      => 1,
            'to'        => 10,
            'useSecondaryLanguage' => 'True'
        ];
    }

    /**
    * @test
    */
    public function it_will_get_all_hotels()
    {
        $request = [
            'fields'            => 'all',
            'destinationCode'   => 'MNL',
            'countryCode'       => 'PH',
            'from'              => 1,
            'to'                => 10,
            'useSecondaryLanguage' => "True"
        ];

        $response = $this->repository->getHotels($request);

        $this->assertCount(10, $response->hotels);
    }

    /**
    * @test
    */
    public function it_will_get_hotel_detail_by_code()
    {
        $code = 85248;

        $request = [
            'useSecondaryLanguage' => "True"
        ];

        $response = $this->repository->getHotelDetails($code, $request);

        $this->assertEquals($code, $response->hotel->code);
    }

    /**
    * @test
    */
    public function it_will_get_countries()
    {
        $request = [
            'fields'=> 'all',
            'code'  => '',
            'from'  => 1,
            'to'    => 100,
            'useSecondaryLanguage' => 'True'
        ];

        $response = $this->repository->getCountries($request);

        $this->assertCount(100, $response->countries);
    }

    /**
    * @test
    */
    public function it_will_get_destinations_by_country_code()
    {
        $request = [
            'fields'        => 'all',
            'countryCodes'  => 'US',
            'from'          => 1,
            'to'            => 10,
            'useSecondaryLanguage' => "True"
        ];

        $response = $this->repository->getDestinations($request);

        $this->assertCount(10, $response->destinations);
    }

    /**
    * @test
    */
    public function it_will_get_accommodations()
    {
        $response = $this->repository->getAccommodations($this->request);

        $this->assertArrayHasKey('accommodations',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_boards()
    {
        $response = $this->repository->getBoards($this->request);

        $this->assertArrayHasKey('boards', (array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_categories()
    {
        $response = $this->repository->getCategories($this->request);

        $this->assertArrayHasKey('categories',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_chains()
    {
        $response = $this->repository->getChains($this->request);

        $this->assertArrayHasKey('chains',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_currencies()
    {
        $response = $this->repository->getCurrencies($this->request);

        $this->assertArrayHasKey('currencies',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_facilities()
    {
        $response = $this->repository->getFacilities($this->request);

        $this->assertArrayHasKey('facilities',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_facility_groups()
    {
        $response = $this->repository->getFacilityGroups($this->request);

        $this->assertArrayHasKey('facilityGroups',(array) $response);
    }

    /**
     * @test
     */
    public function it_will_get_facility_typologies()
    {
        $response = $this->repository->getFacilityTypologies($this->request);

        $this->assertArrayHasKey('facilityTypologies',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_issues()
    {
        $response = $this->repository->getIssues($this->request);

        $this->assertArrayHasKey('issues',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_languages()
    {
        $response = $this->repository->getLanguages($this->request);

        $this->assertArrayHasKey('languages',(array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_promotions()
    {
        $response = $this->repository->getPromotions($this->request);

        $this->assertArrayHasKey('promotions',(array) $response);
    }

    /**
     * @test
     */
    public function it_will_get_segments()
    {
        $response = $this->repository->getSegments($this->request);

        $this->assertArrayHasKey('segments',(array) $response);
    }

    /**
     * @test
     */
    public function it_will_get_terminals()
    {
        $response = $this->repository->getTerminals($this->request);

        $this->assertArrayHasKey('terminals',(array) $response);
    }

    /**
     * @test
     */
    public function it_will_get_image_types()
    {
        $response = $this->repository->getImageTypes($this->request);

        $this->assertArrayHasKey('imageTypes',(array) $response);
    }

    /**
     * @test
     */
    public function it_will_get_group_categories()
    {
        $response = $this->repository->getGroupCategories($this->request);

        $this->assertArrayHasKey('groupCategories',(array) $response);
    }

    /**
     * @test
     */
    public function it_will_get_rate_comments()
    {
        $response = $this->repository->getRateComments($this->request);

        $this->assertArrayHasKey('rateComments',(array) $response);
    }

    /**
     * @test
     */
    public function it_will_get_rate_comment_details()
    {
        $this->request['code'] = '1|100038|3';
        $this->request['date'] = date('Y-m-d');

        $response = $this->repository->getRateCommentDetails($this->request);

        $this->assertArrayHasKey('rateComments',(array) $response);
    }
}