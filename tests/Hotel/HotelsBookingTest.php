<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 30/01/2019
 * Time: 10:35 AM
 */

namespace tests\Hotel;


use HotelBeds\Hotel\HotelsBooking;
use HotelBeds\Hotel\Requests\AvailabilityRequest;
use HotelBeds\Hotel\Requests\BookingRequest;
use HotelBeds\Hotel\Requests\CheckRateRequest;
use PHPUnit\Framework\TestCase;
use tests\HotelTestCase;

class HotelsBookingTest extends HotelTestCase
{
    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        \Dotenv\Dotenv::create(__DIR__,'../../.env')->load();
        $this->repository = new HotelsBooking(getenv('HOTELBEDS_URL'),getenv('HOTELBEDS_KEY'),getenv('HOTELBEDS_SECRET'));
    }

    /**
    * @test
    */
    public function it_get_available_hotels()
    {
        $response = $this->getAvailability();

        $this->assertArrayHasKey('hotels', (array) $response);

    }

    /**
    * @test
    */
    public function it_will_recheck_rate_selected()
    {
        $hotel = $this->getAvailability();

        foreach($hotel->hotels->hotels[0]->rooms[0]->rates as $rate)
        {
            if($rate->rateType === 'RECHECK')
            {
               $rates[] = ['rateKey' => $rate->rateKey];

               break;
            }
        }

        if(!isset($rates)){
            $rates[] = ['rateKey' => $hotel->hotels->hotels[0]->rooms[0]->rates[0]->rateKey];
        }

        $request = new CheckRateRequest();

        $request->setRateKey($rates);

        $response = $this->repository->checkRates($request);

        $this->assertArrayHasKey('hotel', (array) $response);
        $this->assertCount(1, $response->hotel->rooms[0]->rates);
    }

    /**
    * @test
    */
    public function it_send_booking_request_for_confirmation()
    {
        $hotel = $this->getAvailability();

        $rate = $hotel->hotels->hotels[0]->rooms[0]->rates[0];

        $rateKey = $rate->rateKey;

        if($rate->rateType === 'RECHECK'){
            $request = new CheckRateRequest();

            $rates[] = ['rateKey' => $rate->rateKey];

            $request->setRateKey($rates);

            $response = $this->repository->checkRates($request);

            $rateKey = $response->hotel->rooms[0]->rates[0]->rateKey;
        }

        $booking = [
            'holder' => [
                'name'        => 'Alvin',
                'surname'    => 'Cacayurin'
            ],
            'rooms' => [
                0 => [
                    'rateKey' => $rateKey,
                    'paxes' => [
                        0 => [
                            'roomId'    => 1,
                            'type'      => 'AD',
                            'name'      => 'Alvin',
                            'surname'   => 'Cacayurin'
                        ],
                        1 => [
                            'roomId'    => 1,
                            'type'      => 'AD',
                            'name'      => 'Alvin2',
                            'surname'   => 'Cacayurin'
                        ],
                        2 => [
                            'roomId'    => 1,
                            'type'      => 'CH',
                            'name'      => 'AlvinJr',
                            'surname'   => 'Cacayurin',
                            'age'       => 5
                        ]
                    ]
                ]
            ]
        ];

        $request = new BookingRequest();

        $request->setHolder($booking['holder'])
            ->setRooms($booking['rooms']);

        $response = $this->repository->booking($request);

        $this->assertArrayHasKey('booking', (array) $response);

    }

    /**
    * @test
    */
    public function it_will_get_list_of_booking_transaction()
    {
        $search = [
            'start'     => date('Y-m-d'),
            'end'       => date('Y-m-d'),
            'filterType' => 'CREATION',
            'status'    => 'ALL',
            'from'      => 1,
            'to'        => 25
        ];

        $response = $this->repository->bookingList($search);

        $this->assertArrayHasKey('bookings', (array) $response);
    }

    /**
    * @test
    */
    public function it_will_get_detail_of_booking_details()
    {
        $search = [
            'start'     => date('Y-m-d'),
            'end'       => date('Y-m-d'),
            'filterType' => 'CREATION',
            'status'    => 'CONFIRMED',
            'from'      => 1,
            'to'        => 1
        ];

        $booking = $this->repository->bookingList($search)->bookings->bookings;

        $response = $this->repository->bookingDetail($booking[0]->reference);

        $this->assertArrayHasKey('booking', (array) $response);

    }

    /**
    * @test
    */
    public function it_will_cancel_booking_transaction()
    {
        $search = [
            'start' => date('Y-m-d'),
            'end' => date('Y-m-d'),
            'filterType' => 'CREATION',
            'status' => 'CONFIRMED',
            'from' => 1,
            'to' => 1
        ];

        $booking = $this->repository->bookingList($search)->bookings->bookings[0];

        $cancellation = [
            'cancellationFlag' => 'CANCELLATION'
        ];

        $response = $this->repository->bookingCancellation($booking->reference, $cancellation);

        $this->assertArrayHasKey('booking', (array)$response);
    }

}