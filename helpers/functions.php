<?php

function print_arr($value)
{
    return "<pre>".print_r($value)."</pre>";
}

function print_json($value)
{
    return "<pre>".print_r(json_decode(json_encode($value)))."</pre>";
}

function getUnEmptyValue($data)
{
    return array_filter($data,function($value){return $value;});
}